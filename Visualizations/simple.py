import numpy as np
import pybullet as p
import sys
#sys.path.append("/HPS/Shimada/work/rbdl35/build/python") #/usr/lib/x86_64-linux-gnu/l
import rbdl
#import math
#import torch
#import cv2
#from scipy.ndimage import gaussian_filter1d
#sys.path.append("../util")
import Utils.misc as ut
from Utils.initializer import InitializerConsistentHumanoid2
from Utils.angles import angle_util
import time
import copy
id_simulator = p.connect(p.GUI)
p.configureDebugVisualizer(flag=p.COV_ENABLE_Y_AXIS_UP, enable=1)
p.configureDebugVisualizer(flag=p.COV_ENABLE_SHADOWS, enable=0)
target_joints = [ "head", "neck",   "left_knee", "left_ankle", "left_toe",  "right_knee",  "right_ankle", "right_toe",  "left_shoulder", "left_elbow", "left_wrist", "right_shoulder", "right_elbow", "right_wrist"]
AU = angle_util()
ini = InitializerConsistentHumanoid2(1, target_joints)

rbdl2bullet = ini.get_rbdl2bullet()
def get_qs_gt( path):
    gt_qs = np.load(path)[1:]
    gt_qs[:, 7:] = gt_qs[:, 7:][:, rbdl2bullet]
    gt_qs[:, 7:] = np.array([np.array(list(map( AU.angle_clean, x))) for x in gt_qs[:, 7:]])
    return gt_qs

K = np.array([752.6881, 0, 517.431, 0, 0, 752.8311, 500.631, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]).reshape(4,4)[:3,:3]
RT = np.array([ -0.0121125, 0.0119637, -0.9998551, -708.5256, 0.07398777, -0.9971766, -0.0128279, 887.3783, -0.9971856, -0.07413242, 0.01119314, 3340.92, 0, 0, 0, 1 ]).reshape(4,4)
RT_outdoor2 = np.array([--0.0753311, 0.007644453, -0.9971293, 117.6134, -0.03987908, -0.9991937, -0.004647529, 1435.971, -0.9963609 ,0.0394145, 0.07557522, 4628.016 ,0, 0, 0 ,1 ]).reshape(4, 4)


R=RT[:3,:3].T
T=RT[:-1,3:].reshape(3)/1000

#gt_qs = get_qs_gt('./generatedData/q_deep_cap/Lan/test/bullet_q_cam.npy')
urdf_base_path = "./URDF/"  #S6.urdf"
""" DeepCap dataset """
model0 = rbdl.loadModel((urdf_base_path+'SMILIN_manual.urdf').encode(), floating_base=True)
model0.gravity = np.array([0., 0, 0.])
id_robot = p.loadURDF(urdf_base_path+'SMILIN_manual.urdf',useFixedBase=False)
#floor_path = "../assets/plane.urdf"
#floor = p.loadURDF(floor_path, [0, 0.0, 0.0], [-0.7071068, 0, 0, 0.7071068], globalScaling=2)
_, _, jointIds, jointNames = ut.get_jointIds_Names(id_robot)
epoch=195
file_name ="SMILIN3_1"#"cropped_Vlad_1_cam1"
#dataHC= np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_fix/results/Outdoor2/cropped_Soshi_1/"+str(epoch)+"_q.npy", mmap_mode='r')
#dataHC= np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_fix/results/DC/HM_MPI_trained/"+str(epoch)+"_q.npy", mmap_mode='r')
#dataHC= np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_inferences/results/SMILIN3_1/"+str(epoch)+"_q3.npy", mmap_mode='r')
#dataHC= np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_fix/results/DC/A_O_C_T/"+str(80)+"_q.npy", mmap_mode='r')
#dataHC = np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_fix/results/DC/A_O_C_T_aug/80_q.npy", mmap_mode='r')
#dataHC = np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_pipeline/results/"+file_name+"/b_0_q_kin.npy", mmap_mode='r')
#dataHC= np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_fix/results/DC/A_O_C_T/"+str(35)+"_q.npy", mmap_mode='r')
#\\wsl.localhost\Ubuntu\home\sshimada\Desktop\NeuralPhysCap\neuralphyscapdemo\results\SMILIN3_1
#dataHC = np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_pipeline/results/"+file_name+"/i_6_b_0_q_dyn.npy", mmap_mode='r')
dataHC = np.load("./results/"+file_name+"/i_6_b_0_q_iter_dyn.npy", mmap_mode='r')

#dataHC = np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_pipeline/results/"+file_name+"/i_8_q_dyn.npy", mmap_mode='r')
##2_vlad_kick_small Discus_Throw4 Discus_Throw6 golf1 Throw_Discus1
#dataHC = np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_pipeline/results/MPI3DHP/TS1/i_8_q_dyn.npy", mmap_mode='r')
#dataHC = np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/clean_pipeline/results/cropped_Soshi_1_cam1/i_6_b_0.0__q_dyn.npy", mmap_mode='r')
#dataHC = np.load("P:/PhysicsHuman2/work/DeepPhysCap/DPC/tmp/pycharm_project_402/DeepPhysCapBatch/irregular_angle_lab/results/DC/"+str(epoch)+"_q.npy", mmap_mode='r')
#dataHC = np.load("P:/PhysicsHuman3/work/DeepPhysCap/predictions/Human36M/openpose_HM/S9/Walking_0/q_camera1_kin.npy", mmap_mode='r')
#"/HPS/PhysicsHuman3/work/DeepPhysCap/predictions/Human36M/openpose_HM/"+subject+"/"+action+"/"/winfs-inf/HPS/Shimada/work/relatedWorks/MotioNet/output/rbdl_npys
# A_O_C_T_aug
q=copy.copy(dataHC)
print(q.shape)
ini.change_color_bullet( id_robot, [238 / 255, 0, 198 / 255, 1])
indices = np.arange(0,len(q),4)

q[:,6+27]=0
q[:,6+35]=0
q[:,6+36]=0
q[:,6+37]=0
q[:,6+38]=0
#q[:,:3]=0
print(q.shape)
#q=q[2250:]
count=0
#op_2Ds[i] = gaussian_filter1d(op_2Ds[i], 2)
#q[:,0]=gaussian_filter1d(q[:,0], 2)
#q[:,1]=gaussian_filter1d(q[:,1], 2)
#q[:,2]=gaussian_filter1d(q[:,2], 2)
print(q[:,0].shape)
while(1):
#for i in range(len(q)):
    print(count)
    print(q[count][:3])
    ut.visualization3D_multiple([id_robot], jointIds, rbdl2bullet, [q[count]], R, T, overlay=True)  # q_kin[i],q_pd[i],
    #[p.addUserDebugText("." + str(k), x, [0, 0, 1], textSize=3, replaceItemUniqueId=k + 100) for k, x in enumerate(ankles[i])]
    # [p.addUserDebugText("." + str(k), x, [0, 0, 1], textSize=3, replaceItemUniqueId=k + 100) for k, x in enumerate(p_3ds[i])]

    time.sleep(0.03)

    count+=1
    if count >= len(q):
        count =0
