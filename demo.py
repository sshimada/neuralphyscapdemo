import torch
import sys
import os
#sys.path.append("/HPS/Shimada/work/rbdl37/rbdl/build/python")  # /usr/lib/x86_64-linux-gnu/l
import rbdl
#print(torch.__version__)
import warnings
import time 
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))
print(dir_path)
import numpy as np
from networks import TargetPoseNetArt,TargetPoseNetOri,ContactEstimationNetwork,TransCan3Dkeys,DynamicNetwork,GRFNet
from Utils.angles import angle_util 
import Utils.misc as ut
import Utils.phys3 as ppf
from Utils.core_utils import CoreUtils
from Utils.initializer import InitializerConsistentHumanoid2
from lossFunctions import LossFunctions
import pybullet as p
import Utils.contacts as cut
from pipeline_util import  PyProjection,PyPerspectivieDivision
from scipy.spatial.transform import Rotation as Rot
from torch.autograd import Variable
import argparse  

class InferencePipeline():
    def __init__(self,urdf_path,net_path,file_name,data_path,save_base_path,w,h,K,RT,neural_PD=1,grad_descent=0,bias_correct=0,n_iter=6,con_thresh=0.01,limit=50,speed_limit=35):

        ### configuration ###
        self.w=w
        self.h=h
        self.neural_PD=neural_PD
        self.bias_correct = bias_correct
        self.n_iter = n_iter
        self.con_thresh = con_thresh
        self.limit = limit
        self.speed_limit = speed_limit
        self.grad_descent = grad_descent
        self.save_base_path=save_base_path
        self.file_name=file_name
        self.n_iter_GD=90

        ### joint mapping ###
        self.openpose_dic2 = { "base": 7, "left_hip": 11, "left_knee": 12, "left_ankle": 13, "left_toe": 19, "right_hip": 8, "right_knee": 9, "right_ankle": 10, "right_toe": 22, "neck": 0, "head": 14, "left_shoulder": 4, "left_elbow": 5, "left_wrist": 6, "right_shoulder": 1, "right_elbow": 2,  "right_wrist": 3 }
        self.target_joints = ["head", "neck", "left_hip", "left_knee", "left_ankle", "left_toe", "right_hip", "right_knee", "right_ankle", "right_toe", "left_shoulder", "left_elbow", "left_wrist", "right_shoulder", "right_elbow", "right_wrist"]
        self.target_ids = [self.openpose_dic2[key] for key in self.target_joints]

        ### load humanoid model ###

        self.model = rbdl.loadModel(urdf_path.encode(), floating_base=True)
        self.id_robot = p.loadURDF(urdf_path, useFixedBase=False)

        ### initilization ###
        ini = InitializerConsistentHumanoid2(n_b, self.target_joints)
        self.rbdl_dic = ini.get_rbdl_dic()
        self.target_joint_ids = ini.get_target_joint_ids()
        _, _, jointIds, jointNames = ut.get_jointIds_Names(self.id_robot)
        self.model_addresses = {"0": self.model, "1": self.model}

        ### build and load pretrained models ###
        self.TempConvNetArt = TargetPoseNetArt(in_channels=32, num_features=1024, out_channels=40, num_blocks=4)#.cuda()
        self.TempConvNetOri = TargetPoseNetOri(in_channels=32, num_features=1024, out_channels=4, num_blocks=4)#.cuda()
        self.ConNet = ContactEstimationNetwork(in_channels=32, num_features=1024, out_channels=4, num_blocks=4)#.cuda()
        self.TempConvNetTrans = TransCan3Dkeys(in_channels=32, num_features=1024, out_channels=3, num_blocks=4)#.cuda()
        self.GRFNet = GRFNet(input_dim=577, output_dim=46 + 46 + 3 * 4)#.cuda()
        self.DyNet = DynamicNetwork(input_dim=2302, output_dim=46, offset_coef=10)#.cuda()

        if os.path.exists(net_path + "ArtNet_" + str(95) + ".pkl"): 
            self.TempConvNetArt.load_state_dict(torch.load(net_path + "ArtNet_" + str(95) + ".pkl",  map_location=torch.device('cpu')))
            self.TempConvNetOri.load_state_dict(torch.load(net_path + "OriNet_" + str(95) + ".pkl", map_location=torch.device('cpu')))
            self.ConNet.load_state_dict(torch.load(net_path + "ConNet_" + str(95) + ".pkl", map_location=torch.device('cpu')))
            self.GRFNet.load_state_dict(torch.load(net_path + "GRFNet_" + str(25) + ".pkl" ,map_location=torch.device('cpu')))
            self.TempConvNetTrans.load_state_dict(torch.load(net_path+"TransNetOpenPose3DLossOnly_" + str(95) + ".pkl", map_location=torch.device('cpu')))
            self.DyNet.load_state_dict(torch.load(net_path+ "DyNet_" + str(4) + ".pkl", map_location=torch.device('cpu'))) 
        else:
            print('no trained model found!!!')
            sys.exit()
        self.TempConvNetArt.eval()
        self.TempConvNetOri.eval()
        self.TempConvNetTrans.eval()
        self.ConNet.eval()
        self.DyNet.eval()
        self.GRFNet.eval()

        ### setup custom pytorch functions including the Physics model ###PyForwardKinematicsQuaternionHuman36M_test
        self.PyFK = ppf.PyForwardKinematicsQuaternionHuman36M_test().apply 
        self.PyFK_rr = ppf.PyForwardKinematicsQuaternionHuman36M_test().apply 
        self.PyFD = ppf.PyForwardDynamics.apply
        self.PyProj =  PyProjection.apply
        self.PyPD =   PyPerspectivieDivision.apply

        ### load input data ###
        self.RT=RT
        self.Rs = torch.FloatTensor(self.RT[:3, :3]).view(n_b, 3, 3)
        self.P = torch.FloatTensor(K[:3])
        self.P_tensor = self.get_P_tensor(n_b, self.target_joint_ids, self.P)
        self.p_2ds = np.load(data_path)

        self.p_2d_basee = self.p_2ds[:, self.openpose_dic2["base"]]
        self.p_2ds = self.p_2ds[:, self.target_ids]
        self.p_2ds = torch.FloatTensor(self.p_2ds)
        self.p_2d_basee = torch.FloatTensor(self.p_2d_basee)

        self.canoical_2Ds = self.canonicalize_2Ds(torch.FloatTensor(K[:3, :3]), self.p_2ds)
        self.p_2ds[:, :, 0] /= self.w
        self.p_2ds[:, :, 1] /= self.h  # h
        self.p_2d_basee[:, 0] /= self.w
        self.p_2d_basee[:, 1] /= self.h  # h
        self.p_2ds_rr = self.p_2ds - self.p_2d_basee.view(-1, 1, 2)

    def get_P_tensor(self,N, target_joint_ids, P):
        P_tensor = torch.zeros(N, 3 * len(target_joint_ids), 4 * len(target_joint_ids))
        for i in range(int(P_tensor.shape[1] / 3)):
            P_tensor[:, i * 3:(i + 1) * 3, i * 4:(i + 1) * 4] = P
        return torch.FloatTensor(np.array(P_tensor))

    def canonicalize_2Ds(self,K, p_2Ds):
        cs = torch.FloatTensor([K[0][2], K[1][2]]).view(1, 1, 2)
        fs = torch.FloatTensor([K[0][0], K[1][1]]).view(1, 1, 2)
        canoical_2Ds = (p_2Ds - cs) / fs
        return canoical_2Ds

    def get_grav_corioli(self,sub_ids, floor_noramls, q, qdot):
        n_b, _ = q.shape
        q = q.cpu().numpy().astype(float)
        qdot = qdot.cpu().numpy().astype(float)
        gcc = np.zeros((n_b, self.model.qdot_size))
        floor_noramls = floor_noramls.cpu().numpy()
        for batch_id in range(n_b):
            sid = sub_ids[batch_id]
            model_address = self.model_addresses[str(int(sid))]
            model_address.gravity = -9.8 * floor_noramls[batch_id]
            rbdl.InverseDynamics(model_address, q[batch_id], qdot[batch_id], np.zeros(self.model.qdot_size).astype(float),  gcc[batch_id])

        return torch.FloatTensor(gcc)

    def get_mass_mat(self,model, q):
        n_b, _ = q.shape
        M_np = np.zeros((n_b, model.qdot_size, model.qdot_size))
        [rbdl.CompositeRigidBodyAlgorithm(model, q[i].astype(float), M_np[i]) for i in range(n_b)]
        return M_np

    def vnect_mdd_loader(self,filename):
        with open(filename) as f:
            content = f.readlines()
        content = np.array([ x.strip().split("," ) for x in content][1:])
        content = np.array([ np.array(list(map(float,x)))  for x in content] )[:,1:].reshape(len(content),-1,2)
        return content

    def contact_label_estimation(self,input_rr):
        pred_labels = self.ConNet(input_rr)
        pred_labels = pred_labels.clone().cpu()
        pred_labels_prob = pred_labels.clone()
        pred_labels[pred_labels < self.con_thresh] = 0
        pred_labels[pred_labels >= self.con_thresh] = 1
        return pred_labels,pred_labels_prob

    def gradientDescent(self,trans0,target_2D,rr_3ds):
        trans_variable = trans0.clone()
        for j in range(self.n_iter_GD):
            trans_variable = Variable(trans_variable, requires_grad=True)
            p_3D =(rr_3ds.view(n_b,-1,3)+trans_variable.view(n_b,1,3)).view(n_b,-1)

            p_proj = self.PyProj(self.P_tensor, p_3D)
            p_2D = self.PyPD(p_proj)
            p_2D = p_2D.view(n_b,-1,2)
            p_2D[:,:,0]/=self.w
            p_2D[:,:,1]/=self.h
            loss2D = (p_2D.view(1,  -1) - target_2D.view(1,  -1)).pow(2).sum()
            loss2D.backward()
            with torch.no_grad():
                trans_variable -= 0.003  * trans_variable.grad
                trans_variable.grad.zero_()

            trans_variable = trans_variable.clone().detach()
            p_2D = p_2D.detach() 

        p_2D = p_2D.clone().detach()#*1000
        p_2D = p_2D.view(1, -1, 2)
        #p_2D[:, :, 0] *= self.w
        #p_2D[:, :, 1] *=self.h
        return trans_variable ,p_2D

    def get_translations_GD(self,target_2D,rr_p_3D_p,trans0):
        if self.bias_correct:
            rr_p_3D_p = torch.transpose(torch.bmm(self.R_ad, torch.transpose(rr_p_3D_p.view(n_b, -1, 3), 1, 2)), 1, 2).reshape(n_b, -1)

        """ set 2D and 3D targets """ 
        trans, _ = self.gradientDescent( trans0, target_2D, rr_p_3D_p.view(n_b, -1))

        #target_2D = target_2D.view(n_b, -1, 2)
        #target_2D[:, :, 0] *= self.w
        #target_2D[:, :, 1] *= self.h

        if self.bias_correct:
            ### adjust the orientation bias ###
            p_3Ds = torch.transpose(torch.bmm(self.R_ad, torch.transpose(rr_p_3D_p, 1, 2)), 1, 2) + trans.view(1, 1, 3) + torch.FloatTensor( [0, 0.1, 0.3]).view(1, 1, 3)  # .cuda()

        return trans.clone()

    def get_target_pose(self,input_can,input_rr,target_2d,trans0,first_frame_flag):
        art_tar = self.TempConvNetArt(input_rr)
        quat_tar = self.TempConvNetOri(input_rr)
        rr_q = torch.cat((torch.zeros(n_b, 3) , quat_tar[:, 1:], art_tar, quat_tar[:, 0].view(-1, 1)), 1)#.cuda()
        #rr_p_3D_p = self.PyFK_rr(torch.FloatTensor([0]), rr_q) 
        rr_p_3D_p = self.PyFK_rr([self.model_addresses["0"]], self.target_joint_ids,delta_t, torch.FloatTensor([0]) , rr_q)
        #PyFK.apply(model,target_joint_ids,delta_t, torch.FloatTensor([0]) , Variable(torch.zeros(47).cuda().view(1,-1), requires_grad=True))
        q_tar = rr_q.clone()

        if not first_frame_flag and self.grad_descent:
            trans_tar = self.get_translations_GD(target_2d.cpu(),rr_p_3D_p.cpu().detach(),trans0.cpu().detach())
        else:
            if self.bias_correct:
                rr_p_3D_p = torch.transpose(torch.bmm(self.R_ad, torch.transpose(rr_p_3D_p.view(1, -1, 3), 1, 2)), 1, 2).reshape(1, -1)

                quat_tar_np = quat_tar.detach().cpu().numpy()
                r = Rot.from_quat([quat_tar_np[0][1],quat_tar_np[0][2],quat_tar_np[0][3],quat_tar_np[0][0]])
                ori_mat = r.as_matrix()
                ori_mat = np.dot(self.R_ad[0].detach().cpu().numpy(),ori_mat)
                r2=Rot.from_matrix(ori_mat)
                quat_tar_np_dif_order= r2.as_quat()
                quat_tar=torch.FloatTensor(np.array([quat_tar_np_dif_order[-1],quat_tar_np_dif_order[0],quat_tar_np_dif_order[1],quat_tar_np_dif_order[2]])).view(1,-1)#.cuda()
            trans_tar = self.TempConvNetTrans(input_can, rr_p_3D_p)
            trans_tar = torch.clamp(trans_tar, -50, 50)

        q_tar[:, :3] = trans_tar.clone()
        return art_tar,quat_tar,trans_tar,q_tar

    def inference(self):

        ### Initializatoin ###
        all_q ,all_labels,all_labels,all_p_3ds,all_q_kin,all_GRF,all_tau,all_3Ds,all_time,all_iter_q,all_p_3ds_kin,all_label_prob= [],[],[],[],[],[],[],[],[],[],[],[]
        max_count =   len(self.p_2ds_rr) - 3
        p_2ds_rr = self.p_2ds_rr#.cuda()
        canoical_2Ds = self.canoical_2Ds#.cuda()
        p_2ds = self.p_2ds#.cuda()
        ### set rotation for bias correcton ###
        r = Rot.from_euler('xyz', [-0.1, 0, 0])  # -0.3
        self.R_ad = torch.FloatTensor(r.as_matrix()).view(1, 3, 3)#.cuda()
        ### set axis vectors ###
        basis_vec_w = torch.FloatTensor(np.array([[1, 0, 0, ], [0, 1, 0, ], [0, 0, 1, ]])).view(1, 3, 3)
        basis_vec_w = basis_vec_w.expand(n_b, -1, -1)

        for i in range(temporal_window, 30):#len(p_2ds_rr)):
            print(i)
            start = time.time()
            frame_canonical_2Ds = canoical_2Ds[i - temporal_window:i, ].reshape(n_b, temporal_window, -1)
            frame_rr_2Ds = p_2ds_rr[i - temporal_window:i, ].reshape(n_b, temporal_window, -1)
            floor_noramls = torch.transpose(torch.bmm(self.Rs, torch.transpose(basis_vec_w, 1, 2)), 1, 2)[:, 1].view(n_b, 3)
            input_rr = frame_rr_2Ds.reshape(n_b, temporal_window, -1)
            input_can = frame_canonical_2Ds.reshape(n_b, temporal_window, -1)
            target_2d = p_2ds[i].reshape(n_b,-1)
            if i==temporal_window:
                tar_trans0 = None 
                first_frame_flag=1
            else:
                first_frame_flag=0

            ### compute Target Pose ###
            art_tar, quat_tar, trans_tar, q_tar = self.get_target_pose(input_can,input_rr,target_2d,tar_trans0,first_frame_flag)
            tar_trans0=trans_tar.clone()
            with torch.no_grad():
                ### adjust the orientation bias ###
                #if self.bias_correct:
                #     rr_p_3D_p = torch.transpose(torch.bmm(self.R_ad, torch.transpose(rr_p_3D_p.view(1, -1, 3), 1, 2)), 1, 2).reshape(1, -1)

                ### compute contact labels ###
                pred_labels,pred_labels_prob = self.contact_label_estimation(input_rr)

                if i == temporal_window:
                    q0 = q_tar.clone().cpu()
                    pre_lr_th_cons = torch.zeros(n_b, 4 * 3)
                    qdot0 = torch.zeros(n_b, self.model.qdot_size)

                quat_tar = quat_tar.cpu()
                art_tar = art_tar.cpu()
                trans_tar = trans_tar.cpu()
                q_tar = q_tar.cpu()

                ### Dynamic Cycle ###
                for iter in range(self.n_iter):
                    ### Compute dynamic quantitites and pose errors ###
                    M = ut.get_mass_mat_cpu(self.model, q0.detach().clone().cpu().numpy())
                    M_inv = torch.inverse(M).clone()
                    M_inv = ut.clean_massMat2(M_inv)
                    J = CU.get_contact_jacobis6D_cpu(self.model, q0.numpy(), [self.rbdl_dic['left_ankle'], self.rbdl_dic['right_ankle']])  # ankles

                    quat0 = torch.cat((q0[:, -1].view(-1, 1), q0[:, 3:6]), 1).detach().clone()
                    errors_trans, errors_ori, errors_art = CU.get_PD_errors_cpu(quat_tar, quat0, trans_tar, q0[:, :3], art_tar, q0[:, 6:-1])
                    current_errors = torch.cat((errors_trans, errors_ori, errors_art), 1)

                    ### Force Vector Computation ###
                    if self.neural_PD:
                        dynInput = torch.cat((q_tar, q0, qdot0, torch.flatten(M_inv, 1), current_errors,), 1)
                        neural_gain, neural_offset = self.DyNet(dynInput )#.cuda()
                        tau = CU.get_neural_development_cpu(errors_trans, errors_ori, errors_art, qdot0, neural_gain.cpu(), neural_offset.cpu(), self.limit,art_only=1, small_z=1)
                    else:
                        tau = CU.get_tau2(errors_trans, errors_ori, errors_art, qdot0, self.limit, small_z=1)

                    gcc = self.get_grav_corioli([0], floor_noramls, q0.clone(), qdot0.clone())
                    tau_gcc = tau + gcc

                    ### GRF computation ###
                    GRFInput = torch.cat((tau_gcc[:, :6], torch.flatten(J, 1), floor_noramls, pred_labels, pre_lr_th_cons), 1)#.cuda()
                    lr_th_cons = self.GRFNet(GRFInput)
                    gen_conF = cut.get_contact_wrench_cpu(self.model, q0, self.rbdl_dic, lr_th_cons.cpu(), pred_labels)

                    ### Forward Dynamics and Pose Update ###
                    tau_special = tau_gcc - gen_conF
                    qddot = self.PyFD(tau_special + gen_conF - gcc, M_inv)
                    quat, q, qdot, _ = CU.pose_update_quat_cpu(qdot0.detach(), q0.detach(), quat0.detach(), delta_t, qddot, self.speed_limit, th_zero=1)

                    qdot0 = qdot.detach().clone()
                    q0 = AU.angle_normalize_batch_cpu(q.detach().clone())
                    if iter == 0: all_tau.append(torch.flatten(tau_special).numpy())
                    all_iter_q.append(torch.flatten(q0).numpy())
                
                ### store the predictions ###
                all_time.append(time.time() - start)
                #[self.model_addresses["0"]], self.target_joint_ids,delta_t, torch.FloatTensor([0]) , rr_q
                p_3D_p = self.PyFK( [self.model_addresses["0"]], self.target_joint_ids,delta_t, torch.FloatTensor([0]) , q0)
                p_3D_p_kin = self.PyFK( [self.model_addresses["0"]], self.target_joint_ids,delta_t, torch.FloatTensor([0]) , q_tar)
                all_q.append(torch.flatten(q0).detach().numpy())
                all_q_kin.append(torch.flatten(q_tar).detach().numpy())
                all_p_3ds.append(p_3D_p[0].cpu().numpy())
                all_p_3ds_kin.append(p_3D_p_kin[0].cpu().numpy())
                all_labels.append(torch.flatten(pred_labels).cpu().numpy())
                all_label_prob.append(torch.flatten(pred_labels_prob).cpu().numpy())
                all_GRF.append(torch.flatten(lr_th_cons).cpu().numpy())

            if i > max_count:
                print(1 / np.average(all_time), 'fps')
                break

        print('saving predictions ...')
        all_q = np.array(all_q)
        all_labels = np.array(all_labels)
        all_GRF = np.array(all_GRF)
        all_iter_q=np.array(all_iter_q)
        all_p_3ds=np.array(all_p_3ds)
        all_p_3ds_kin=np.array(all_p_3ds_kin)
         
        ########### save the predictions ###############
        if not os.path.exists(self.save_base_path + self.file_name + "/"): 
            os.makedirs(self.save_base_path + self.file_name + "/")
        print(self.save_base_path + self.file_name + "/i_"+str(self.n_iter)+"_b_"+str(int(self.bias_correct))+"_q_iter_dyn.npy")
        np.save(self.save_base_path + self.file_name + "/i_"+str(self.n_iter)+"_b_"+str(int(self.bias_correct))+"_q_iter_dyn.npy", all_iter_q)
        np.save(self.save_base_path + self.file_name + "/i_"+str(self.n_iter)+"_b_"+str(int(self.bias_correct))+"_q_dyn.npy", all_q)
        np.save(self.save_base_path + self.file_name + "/b_"+str(self.bias_correct)+"_q_kin.npy", all_q_kin)
        np.save(self.save_base_path + self.file_name + "/i_"+str(self.n_iter)+"_tau.npy", all_tau)
        np.save(self.save_base_path + self.file_name + "/labels_001.npy", all_labels)
        np.save(self.save_base_path + self.file_name + "/labels_prob.npy", all_label_prob)
        np.save(self.save_base_path + self.file_name + "/i_"+str(self.n_iter)+"_b_"+str(int(self.bias_correct))+"_p3D_dyn.npy", all_p_3ds)
        np.save(self.save_base_path + self.file_name + "/b_"+str(int(self.bias_correct))+"_p3D_kin.npy", all_p_3ds_kin)
        np.save(self.save_base_path + self.file_name + "/i_"+str(self.n_iter)+"_b_"+str(int(self.bias_correct))+"_GRF.npy", all_GRF)
        print('Done.')
        return 0


if __name__ == "__main__":
    """
    add
    1) switch in case known params (translnet) unknown (optimization)
    2)
    
    """
    parser = argparse.ArgumentParser(description='arguments for predictions')
    parser.add_argument('--file_name', default='SMILIN3_1')
    parser.add_argument('--n_iter', type=int, default=6)
    parser.add_argument('--con_thresh', type=float, default= 0.001 )
    parser.add_argument('--tau_limit', type=float, default= 80 )
    parser.add_argument('--speed_limit', type=float, default=15)
    parser.add_argument('--bias_correct', type=float, default=0)
    parser.add_argument('--net_path', default="../pretrained_neuralPhys/OP/")
    args = parser.parse_args()

    AU = angle_util()
    LF = LossFunctions()
    delta_t = 0.011
    CU = CoreUtils(45, delta_t)
    warnings.filterwarnings("ignore")
    n_b = 1
    print("time step:", delta_t)
    local = 1
    if local == 1:
        id_simulator = p.connect(p.DIRECT)
        p.configureDebugVisualizer(flag=p.COV_ENABLE_Y_AXIS_UP, enable=1)
        beginning='/home/sshimada/remote'
    else:
        id_simulator = p.connect(p.DIRECT)
        os.environ["CUDA_VISIBLE_DEVICES"] = "0"
        beginning='/HPS'

    file_name = args.file_name 
    save_base_path = "./results/" 
    urdf_path = "./URDF/manual.urdf"
    net_path=args.net_path 
     
    data_path = beginning + "/Shimada/nobackup/youtube_dance/openpose_results/" + file_name + "/npys/" + file_name + '_I_S.npy' 
    #data_path = './sample_data/c_0_2D_interpolate_smooth_headHandled.npy'
    K = np.array([752.6881, 0, 517.431, 0, 0, 752.8311, 500.631, 0, 0, 0, 1, 0, 0, 0, 0, 1]).reshape(4, 4)
    K_simplest_render = np.array([1300, 0, 640, 0, 0, 1300, 360, 0, 0, 0, 1, 0, 0, 0, 0, 1]).reshape(4, 4)#[:3]
    RT = np.array(  [-0.0121125, 0.0119637, -0.9998551, -708.5256, 0.07398777, -0.9971766, -0.0128279, 887.3783, -0.9971856, -0.07413242, 0.01119314, 3340.92, 0, 0, 0, 1]).reshape(4, 4)
    w=1280
    h=720
    temporal_window = 10

    IPL = InferencePipeline(urdf_path,
                            net_path,
                            file_name,
                            data_path,
                            save_base_path,
                            w,h,K_simplest_render,RT,
                            neural_PD=1,
                            grad_descent=1,
                            bias_correct=args.bias_correct,
                            n_iter=args.n_iter,
                            con_thresh=args.con_thresh,
                            limit=args.tau_limit,
                            speed_limit=args.speed_limit)
    IPL.inference()