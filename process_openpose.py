import sys 
import Utils.posenpose_util as opp
import numpy as np
#import json
#import os
#from joblib import Parallel, delayed
#import multiprocessing
local = 0
if local == 1:
    beginning = 'P:'
else:
    beginning = '/HPS'

#openpose_dic={
#    "base":8,"left_hip":12,"left_knee":13,"left_ankle":14, "left_toe":19,"right_hip":9,"right_knee":10,"right_ankle":11,"right_toe":22, "neck":1, "head":0,
#    "left_shoulder":5,"left_elbow":6,"left_wrist":7,"right_shoulder":2, "right_elbow":3,"right_wrist":4}

#target_joints = ['head', "neck", "left_hip", "left_knee", "left_ankle", "left_toe", "right_hip", "right_knee", "right_ankle", "right_toe", "left_shoulder", "left_elbow", "left_wrist", "right_shoulder", "right_elbow", "right_wrist"]
#target_ids = [openpose_dic[key] for key in target_joints]

def process(interpolate,smoothing):
    head_keys = [14, 15, 16, 17]
    neck_key = [0]
    print('--------')
    p_2ds = opp.get_2ds_from_jsons(data_path)
    print('--------')
    p_2ds = opp.handle_head_keypoints(p_2ds, neck_key, head_keys)
    print('--------')
    if interpolate:
        p_2ds = opp.openpose_interpolate(p_2ds)
        print('--------')
    if smoothing:
        p_2ds = opp.openpose_smoothing(p_2ds)
        print('--------')
    return p_2ds

def save_data(save_path,save_name,data):
    np.save(save_path+save_name,data)
    return 0
 
if __name__ == "__main__":
    file_name = "0"
    data_path = "/home/sshimada/remote/Shimada/nobackup/youtube_dance/openpose_results/cartwheel1/json/"
    save_path = "./sample_data/"
    interpolate=1
    smoothing=1
#//winfs-inf/HPS/Shimada/nobackup/youtube_dance/openpose_results/cartwheel1/json/
    p_2ds = process(interpolate,smoothing)#
    save_data(save_path,'c_'+file_name+'_2D_interpolate_smooth_headHandled.npy',p_2ds)
    print(p_2ds.shape)