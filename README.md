# NeuralPhysCapDemo


# PhysCap: Physically Plausible Monocular 3D Motion Capture in Real Time
The implementation is based on [SIGGRAPH '21](http://vcai.mpi-inf.mpg.de/projects/PhysAware/). 
 
## Dependencies
- Python 3.7
- Ubuntu 18.04 (The system should run on other Ubuntu versions and Windows, however not tested.)
- RBDL: Rigid Body Dynamics Library (https://rbdl.github.io/)
- PyTorch 1.8.1 with GPU support 
- For other python packages, please check requirements.txt

## Installation
- Download and install Python binded RBDL from  https://github.com/rbdl/rbdl
- Install Pytorch 1.8.1 with GPU support (https://pytorch.org/) (other versions should also work but not tested)
- Install python packages by:

		pip install -r requirements.txt

## How to Run 
1) Download pretrained model from [here](???). Place all the models under ???.
 
2) We provide a sample data under ???. To run the code on the sample data, first go to ??? directory and run:

    python demo.py  --input_dir /PATH/TO/PROCESSED/NPYfile

3) To visualize the prediction, run:

    python sample.py --input_dir /PATH/TO/OUTPUT --save_dir /????/

E.g., run on the sample data:

    python demo.py  --input_dir /PATH/TO/PROCESSED/NPYfile --save_dir /????/

E.g., visualize the sample output data:

    python sample.py --input_dir /PATH/TO/OUTPUT

## How to Run on Your Data

1) Process your openpose data to be compatible with NeuralPhyscap:
 
		python process_openpose.py --input_dir /PATH/TO/OPENPOSE/JSON/FILE --save_dir /????

This will generate ".npy" file to run NeuralPhyscap.

2) Run NeuralPhyscap on the generated npy file:
	
	First, run the following command to apply preprocessing on the 2D keypoints:

		python demo.py  --input_dir /PATH/TO/PROCESSED/NPYfile --save_dir /????/

 

## License Terms
Permission is hereby granted, free of charge, to any person or company obtaining a copy of this software and associated documentation files (the "Software") from the copyright holders to use the Software for any non-commercial purpose. Publication, redistribution and (re)selling of the software, of modifications, extensions, and derivates of it, and of other software containing portions of the licensed Software, are not permitted. The Copyright holder is permitted to publically disclose and advertise the use of the software by any licensee. 

Packaging or distributing parts or whole of the provided software (including code, models and data) as is or as part of other software is prohibited. Commercial use of parts or whole of the provided software (including code, models and data) is strictly prohibited. Using the provided software for promotion of a commercial entity or product, or in any other manner which directly or indirectly results in commercial gains is strictly prohibited. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Citation
If the code is used, the licesnee is required to cite the use of VNect and the following publication in any documentation 
or publication that results from the work:
```
@article{
	PhysAwareTOG2021,
	author = {Shimada, Soshi and Golyanik, Vladislav and Xu, Weipeng and P\'{e}rez, Patrick and Theobalt, Christian},
	title = {Neural Monocular 3D Human Motion Capture with Physical Awareness},
	journal = {ACM Transactions on Graphics}, 
	month = {aug},
	volume = {40},
	number = {4}, 
	articleno = {83},
	year = {2021}, 
	publisher = {ACM}, 
	keywords = {Monocular 3D Human Motion Capture, Physical Awareness, Global 3D, Physionical Approach}
}
```